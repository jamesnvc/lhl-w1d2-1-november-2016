//
//  Room.h
//  OOPDemoOne
//
//  Created by James Cash on 01-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Room : NSObject

@property NSInteger width;
@property NSInteger height;
@property NSInteger depth;
@property NSInteger windowArea;

- (instancetype)initWithWidth:(NSInteger)width
                    andHeight:(NSInteger)height
                     andDepth:(NSInteger)depth
                andWindowArea:(NSInteger)area;

- (NSInteger)volume;
- (CGFloat)brightness;

- (BOOL)isBrighterThan:(Room*)otherRoom;

@end
