//
//  Room.m
//  OOPDemoOne
//
//  Created by James Cash on 01-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "Room.h"

@implementation Room

- (instancetype)init {
    self = [super init];
    if (self) {
        _width = 10;
        _height = 10;
        _depth = 10;
        _windowArea = 10;
    }
    return self;
}

- (instancetype)initWithWidth:(NSInteger)width andHeight:(NSInteger)height andDepth:(NSInteger)depth andWindowArea:(NSInteger)area
{
    self = [super init];
    if (self) {
        _width = width;
        _height = height;
        _depth = depth;
        _windowArea = area;
    }
    return self;
}

- (NSInteger)volume {
    return self.width * self.height * self.depth;
}

- (CGFloat)brightness {
    return self.windowArea / (1.0 * self.volume);
}

- (BOOL)isBrighterThan:(Room *)otherRoom
{
    CGFloat ourBrightness = self.brightness;
    CGFloat theirBrightness = otherRoom.brightness;
    return ourBrightness > theirBrightness;
}

@end
