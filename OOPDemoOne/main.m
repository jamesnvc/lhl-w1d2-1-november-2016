//
//  main.m
//  OOPDemoOne
//
//  Created by James Cash on 01-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Room.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        // Rooms
        // rooms have - size
        //            - windows

        /*
        int room1Width = 10;
        int room1Depth = 20;
        int room1Height = 10;
        int room1Volume = room1Width * room1Depth * room1Height;
        int room1Area = 50;

        float room1Brightness = room1Area / room1Volume;
        */
        Room *room1 = [[Room alloc] init];
        NSLog(@"Room 1 volume = %ld", room1.volume);
        NSLog(@"Room 1 brightness = %f", [room1 brightness]);

        Room *room2 = [[Room alloc] initWithWidth:5
                                        andHeight:10
                                         andDepth:5
                                    andWindowArea:50];
//        room2.width = 5;
//        room2.depth = 5;
//        room2.height = 10;
//        room2.windowArea = 50;
        NSLog(@"Room 2 volume = %ld", room2.volume);
        NSLog(@"Room 2 brightness = %f", [room2 brightness]);

        if ([room1 isBrighterThan:room2]) {
            NSLog(@"Room one is the brightest");
        } else {
            NSLog(@"Room two is brighter");
        }

        NSMutableArray<Room*> *rooms = [[NSMutableArray alloc] init];
        [rooms addObjectsFromArray:@[room1, room2]];

        for (int i = 0; i < 10; i++) {
            Room* newRoom = [[Room alloc] init];
            [rooms addObject:newRoom];
        }
        NSLog(@"Our array now has %ld rooms", [rooms count]);
        CGFloat brightestSoFar = 0;
        for (Room* room in rooms) {
            NSLog(@"Room %@ has brightness %f", room, room.brightness);
            brightestSoFar = MAX(brightestSoFar, room.brightness);
        }
        NSLog(@"The brightest room was %f", brightestSoFar);
        
        NSArray<NSNumber*> *numbers = @[@(1),@(2),@(3),[NSNumber numberWithInt:4]];
        NSNumber *n1 = numbers[0];
        NSNumber *n2 = numbers[1];
        [n1 intValue] + [n2 intValue] + numbers[2].intValue;

    }
    return 0;
}
